﻿using Contact.API.DI;
using Contact.DAL.Entities;
using Contact.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Contact.API.Controllers
{
    public class UserController : ApiController
    {
        public List<User> Get()
        {
            return ServicesLocator.Instance
                .GetService<IUserRepository>()
                .GetAll().ToList();
        }

        public int Post(User model)
        {
            if (ModelState.IsValid)
            {
                return ServicesLocator.Instance
                .GetService<IUserRepository>()
                .Insert(model);
            }
            throw new HttpResponseException(HttpStatusCode.BadRequest);
        }

        public bool Delete(int id)
        {
            return ServicesLocator.Instance
                .GetService<IUserRepository>()
                .Delete(id);
        }

        public bool Put(User model)
        {
            if (ModelState.IsValid)
            {
                return ServicesLocator.Instance
                        .GetService<IUserRepository>()
                        .Update(model); 
            }
            throw new HttpResponseException(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        [Route("api/User/Relation/{id}")]
        public List<User> GetAllRelations(int id)
        {
            return ServicesLocator.Instance
                .GetService<IUserRepository>()
                .GetAllRel(id).ToList();
        }

        public User Get(int id)
        {
            User u = ServicesLocator.Instance
                .GetService<IUserRepository>()
                .Get(id);
            if (u == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            return u;
        }
    }
}
