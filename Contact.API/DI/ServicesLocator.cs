﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Contact.DAL.Repositories;
using Contact.DAL.Services;
using Microsoft.Extensions.DependencyInjection;
using ToolBox;

namespace Contact.API.DI
{
    public sealed class ServicesLocator
    {
        #region Singleton
        private static ServicesLocator _instance;

        private static object _lock = new object();

        public static ServicesLocator Instance
        {
            get
            {
                lock (_lock)
                {
                    return _instance = _instance ?? new ServicesLocator();
                }
            }
        }

        private ServicesLocator()
        {
            IServiceCollection collection = new ServiceCollection();
            collection.AddSingleton(p => new Connection(
                ConfigurationManager.ConnectionStrings["default"].ProviderName,
                ConfigurationManager.ConnectionStrings["default"].ConnectionString
            ));
            collection.AddSingleton<IUserRepository, UserService>();
            collection.AddSingleton<IRelationshipRepository, RelationshipService>();
            serviceProvider = collection.BuildServiceProvider();
        }
        #endregion

        private readonly IServiceProvider serviceProvider;

        public T GetService<T>()
        {
            return serviceProvider.GetService<T>();
        }
    }
}