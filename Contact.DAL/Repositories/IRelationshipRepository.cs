﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contact.DAL.Repositories
{
    public interface IRelationshipRepository
    {
        int Insert(int userId1, int userId2);
        bool Delete(int relId);
    }
}
