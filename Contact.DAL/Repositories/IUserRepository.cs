﻿using Contact.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contact.DAL.Repositories
{
    public interface IUserRepository
    {
        User Get(int id);
        IEnumerable<User> GetAll();
        int Insert(User u);
        bool Update(User u);
        bool Delete(int id);

        IEnumerable<User> GetAllRel(int id);
        User Login(string email, string pwd);
    }
}
