﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox;

namespace Contact.DAL.Services
{
    public abstract class BaseService
    {
        protected readonly Connection _connection;
        public BaseService(Connection connection)
        {
            _connection = connection;
        }
    }
}
