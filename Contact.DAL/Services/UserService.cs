﻿using Contact.DAL.Entities;
using Contact.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox;

namespace Contact.DAL.Services
{
    public class UserService : BaseService, IUserRepository
    {
        public UserService(Connection connection) : base(connection)
        {
        }

        public bool Delete(int id)
        {
            Command cmd = new Command("DELETE FROM [User] WHERE Id = @id");
            cmd.AddParameter("id", id);
            return _connection.ExecuteNonQuery(cmd) != 0;
        }

        public User Get(int id)
        {
            Command cmd = new Command("SELECT * FROM [User] WHERE Id= @id");
            cmd.AddParameter("id", id);
            return _connection.ExecuteReader(cmd, reader => {
                return new User
                {
                    Id = (int)reader["Id"],
                    LastName = (string)reader["LastName"],
                    FirstName = (string)reader["FirstName"],
                    Email = (string)reader["Email"],
                    // as vérifie si le cast est possible et renvoie le null C# si 
                    // ce n'est pas possible
                    BirthDate = reader["BirthDate"] as DateTime?,
                    Avatar = reader["Avatar"] as byte[]
                };
            }).SingleOrDefault();
        }

        public IEnumerable<User> GetAll()
        {
            Command cmd = new Command("SELECT * FROM [User]");
            return _connection.ExecuteReader(cmd, reader => {
                return new User
                {
                    Id = (int)reader["Id"],
                    LastName = (string)reader["LastName"],
                    FirstName = (string)reader["FirstName"],
                    Email = (string)reader["Email"],
                    // as vérifie si le cast est possible et renvoie le null C# si 
                    // ce n'est pas possible
                    BirthDate = reader["BirthDate"] as DateTime?,
                    Avatar = reader["Avatar"] as byte[]
                };
            });
        }

        public IEnumerable<User> GetAllRel(int id)
        {
            Command cmd = new Command("SP_USER_REL", true);
            cmd.AddParameter("id", id);
            return _connection.ExecuteReader(cmd, reader => {
                return new User
                {
                    Id = (int)reader["Id"],
                    LastName = (string)reader["LastName"],
                    FirstName = (string)reader["FirstName"],
                    Email = (string)reader["Email"],
                    // as vérifie si le cast est possible et renvoie le null C# si 
                    // ce n'est pas possible
                    BirthDate = reader["BirthDate"] as DateTime?,
                    Avatar = reader["Avatar"] as byte[]
                };
            });
        }

        public int Insert(User u)
        {
            Command cmd = new Command("SP_REGISTER", true);
            cmd.AddParameter("lastName", u.LastName);
            cmd.AddParameter("firstName", u.FirstName);
            cmd.AddParameter("email", u.Email);
            cmd.AddParameter("avatar", u.Avatar, DbType.Binary);
            cmd.AddParameter("birthDate", u.BirthDate);
            cmd.AddParameter("password", u.Password);
            u.Id = (int)_connection.ExecuteScalar(cmd);
            return u.Id;
        }

        public User Login(string email, string pwd)
        {
            Command cmd = new Command("SP_LOGIN", true);
            cmd.AddParameter("email", email);
            cmd.AddParameter("password", pwd);
            return _connection.ExecuteReader(cmd, reader => {
                return new User
                {
                    Id = (int)reader["Id"],
                    LastName = (string)reader["LastName"],
                    FirstName = (string)reader["FirstName"],
                    Email = (string)reader["Email"],
                    // as vérifie si le cast est possible et renvoie le null C# si 
                    // ce n'est pas possible
                    BirthDate = reader["BirthDate"] as DateTime?,
                    Avatar = reader["Avatar"] as byte[]
                };
            }).SingleOrDefault();
        }

        public bool Update(User u)
        {
            Command cmd = new Command("SP_USER_UPDATE", true);
            cmd.AddParameter("id", u.Id);
            cmd.AddParameter("lastName", u.LastName);
            cmd.AddParameter("firstName", u.FirstName);
            cmd.AddParameter("birthDate", u.BirthDate);
            cmd.AddParameter("avatar", u.Avatar, DbType.Binary);
            return _connection.ExecuteNonQuery(cmd) != 0;

        }
    }
}
