﻿CREATE PROCEDURE [dbo].[SP_REGISTER]
	@lastName NVARCHAR(50),
	@firstName NVARCHAR(50),
	@email NVARCHAR(255),
	@password NVARCHAR(255),
	@birthDate DATETIME2,
	@avatar VARBINARY(MAX)
AS
	INSERT INTO [User]
	OUTPUT inserted.Id
	VALUES(
		@lastName,
		@firstName,
		@email,
		HASHBYTES('SHA2_512', @password + @email),
		@birthDate,
		@avatar
	)
RETURN 0
