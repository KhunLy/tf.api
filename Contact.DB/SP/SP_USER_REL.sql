﻿CREATE PROCEDURE [dbo].[SP_USER_REL]
	@id int
AS
	SELECT u.* 
	FROM [User] u
	JOIN [Relationship] 
		ON (u.Id = UserId1 OR u.Id = UserId2)
		AND (UserId1 = @id OR UserId2 = @id)
	WHERE u.Id <> @id
RETURN 0
