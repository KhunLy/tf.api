﻿CREATE PROCEDURE [dbo].[SP_USER_UPDATE]
	@id int,
	@lastName NVARCHAR(50),
	@firstName NVARCHAR(50),
	@birthDate DateTime2,
	@avatar VARBINARY(MAX)
AS
	UPDATE [User]
	SET 
		LastName = @lastName,
		FirstName = @firstName,
		BirthDate = @birthDate,
		Avatar = @avatar
	WHERE Id = @id
RETURN 0
