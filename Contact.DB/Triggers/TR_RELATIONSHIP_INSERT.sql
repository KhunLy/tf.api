﻿CREATE TRIGGER [TR_RELATIONSHIP_INSERT]
ON [Relationship]
INSTEAD OF INSERT
AS
BEGIN
	SET NOCOUNT ON
	INSERT INTO Relationship 
	SELECT 
		CASE
			WHEN inserted.UserId1 < inserted.UserId2 THEN inserted.UserId1
			ELSE inserted.UserId2
		END,
		CASE
			WHEN inserted.UserId1 < inserted.UserId2 THEN inserted.UserId2
			ELSE inserted.UserId1
		END,
		inserted.RelationshipDate
	FROM inserted
END
