﻿using Contact.WPF.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Contact.WPF.Services
{
    public class UserProvider
    {
        public async Task<int> RegisterAsync(User model)
        {
            using (HttpClient client = new HttpClient())
            {
                string json = JsonConvert.SerializeObject(model);
                StringContent content = new StringContent(
                    json, 
                    Encoding.UTF8,
                    "application/json"
                );
                HttpResponseMessage response =
                    await client.PostAsync("http://localhost:61706/api/user", content);
                if (response.IsSuccessStatusCode)
                {
                    string message = await response.Content.ReadAsStringAsync();
                    return int.Parse(message);
                }
                return 0;
            }
        }
    }
}
