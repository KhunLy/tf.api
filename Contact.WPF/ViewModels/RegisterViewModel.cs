﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using ToolBox.MVVM.Commands;
using Microsoft.Win32;
using System.IO;
using Contact.WPF.Services;
using Contact.WPF.Models;

namespace Contact.WPF.ViewModels
{
    public class RegisterViewModel
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public byte[] Avatar { get; set; }
        public DateTime? BirthDate { get; set; }

        public RelayCommand BrowseCommand { get; private set; }
        public RelayCommand<PasswordBox> SaveCommand { get; private set; }

        public RegisterViewModel()
        {
            BrowseCommand = new RelayCommand(Browse);
            SaveCommand = new RelayCommand<PasswordBox>(Save);
        }

        private async void Save(PasswordBox obj)
        {
            //throw new NotImplementedException();
            //Contactez l'api et lui envoyer un model
            Password = obj.Password;
            UserProvider pro = new UserProvider();
            await pro.RegisterAsync(new User {
                Password = Password,
                Email = Email,
                Avatar = Avatar,
                LastName = LastName,
                FirstName = FirstName,
                BirthDate = BirthDate
            });
        }

        private void Browse()
        {
            //throw new NotImplementedException();
            // ouvrir une boite de dialog ...
            OpenFileDialog fd = new OpenFileDialog();
            fd.Filter = "Images|*.jpg;*.png;*.jpeg";
            if(fd.ShowDialog() == true)
            {
                // recupérer le tableau de byte
                Avatar = File.ReadAllBytes(fd.FileName);
            }

        }
    }
}
