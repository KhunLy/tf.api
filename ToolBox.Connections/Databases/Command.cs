﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.Connections.Databases;

namespace ToolBox
{
    public class Command
    {
        internal string Query { get; set; }
        internal bool IsStoredProcedure { get; set; }
        internal Dictionary<string, Parameter> Parameters { get; set; }

        public Command(string query, bool isStoredProcedure = false)
        {
            Query = query;
            IsStoredProcedure = isStoredProcedure;
            Parameters = new Dictionary<string, Parameter>();
        }

        public void AddParameter(string parameterName, object value, 
            DbType? type = null)
        {
            Parameters.Add(parameterName, new Parameter {
                Value = value ?? DBNull.Value,
                Type = type
            });
        }

    }
}
