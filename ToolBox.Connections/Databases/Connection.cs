﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.Connections.Databases;

namespace ToolBox
{
    public class Connection
    {
        private string _connectionString;
        private DbProviderFactory _factory;

        public Connection(string providerName, string ConnectionString)
        {
            _factory = DbProviderFactories.GetFactory(providerName);
            _connectionString = ConnectionString;

            using (DbConnection connection = CreateConnection())
            {
                connection.Open();
            }            
        }

        private DbConnection CreateConnection()
        {
            DbConnection c = _factory.CreateConnection();
            c.ConnectionString = _connectionString;
            return c;
        }

        public DbCommand CreateCommand(Command cmd, DbConnection connection)
        {
            DbCommand SqlCmd = connection.CreateCommand();
            SqlCmd.CommandText = cmd.Query;

            if(cmd.IsStoredProcedure)
                SqlCmd.CommandType =  CommandType.StoredProcedure;


            foreach (KeyValuePair<string, Parameter> param in cmd.Parameters)
            {
                DbParameter p = _factory.CreateParameter();
                p.ParameterName = param.Key;
                p.Value = param.Value.Value ?? DBNull.Value;
                if(param.Value.Type != null)
                {
                    p.DbType = (DbType)param.Value.Type;
                }

                SqlCmd.Parameters.Add(p);
            }

            return SqlCmd;
        }

        public dynamic ExecuteScalar(Command command)
        {
            using (DbConnection c = CreateConnection())
            {
                using (DbCommand cmd = CreateCommand(command, c))
                {
                    c.Open();
                    object t = cmd.ExecuteScalar();
                    return (t is DBNull) ? null : t;
                }
            }
        }

        public int ExecuteNonQuery(Command command)
        {
            using (DbConnection c = CreateConnection())
            {
                using (DbCommand cmd = CreateCommand(command, c))
                {
                    c.Open();
                    return cmd.ExecuteNonQuery();
                }
            }
        }

        public DataTable GetDataTable(Command command)
        {
            using (DbConnection c = CreateConnection())
            {
                using (DbCommand cmd = CreateCommand(command, c))
                {
                    using (DbDataAdapter da = _factory.CreateDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        return dt;
                    }
                }
            }
        }

        public IEnumerable<T> ExecuteReader<T>(Command command, Func<IDataRecord, T> selector)
        {
            using (DbConnection c = CreateConnection())
            {
                using (DbCommand cmd = CreateCommand(command, c))
                {
                    c.Open();
                    using (DbDataReader dr = cmd.ExecuteReader())
                    {
                        while(dr.Read())
                        {
                            yield return selector(dr);
                        }
                    }
                }
            }
        }
    }
}
