﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ToolBox.MVVM.Bindable
{
    public abstract class BindableBase: INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual bool SetValue<T>(
            ref T oldValue,
            T newValue,
            [CallerMemberName] string propName = ""
        )
        {
            if(!EqualityComparer<T>.Default.Equals(oldValue, newValue))
            {
                oldValue = newValue;
                RaisePropertyChanged(propName);
                return true;
            }
            return false;
        }

        protected virtual void RaisePropertyChanged
            ([CallerMemberName] string propName = "")
        {
            PropertyChanged?.Invoke(
                this, new PropertyChangedEventArgs(propName));
        }
    }
}
