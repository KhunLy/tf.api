﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ToolBox.MVVM.Commands
{
    public class RelayCommand<T> : ICommand
    {
        public event EventHandler CanExecuteChanged;

        public Action<T> _Callback { get; set; }
        public Func<bool> _CanExecute { get; set; }

        public RelayCommand
            (Action<T> callback, Func<bool> canExecute = null)
        {
            _Callback = callback ?? throw new ArgumentException();
            _CanExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return _CanExecute?.Invoke() ?? true;
        }

        public void Execute(object parameter)
        {
            if (!(parameter is T))
                throw new ArgumentException();
            if (CanExecute(null))
                _Callback?.Invoke((T)parameter);
        }

        public void RaiseCanExecuteChanged(object sender)
        {
            CanExecuteChanged?.Invoke(sender, new EventArgs());
        }
    }
}
