﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ToolBox.MVVM.Commands
{
    public class RelayCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        public Action _Callback { get; set; }
        public Func<bool> _CanExecute { get; set; }

        public RelayCommand
            (Action callback, Func<bool> canExecute = null)
        {
            _Callback = callback ?? throw new ArgumentException();
            _CanExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return _CanExecute?.Invoke() ?? true;
        }

        public void Execute(object parameter)
        {
            if(CanExecute(null))
                _Callback?.Invoke();
        }

        public void RaiseCanExecuteChanged(object sender)
        {
            CanExecuteChanged?.Invoke(sender, new EventArgs());
        }
    }
}
